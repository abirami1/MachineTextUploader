﻿using Mono.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MachineTextUploader
{
    public partial class Form1 : Form
    {
        string[] machine_array = { "EM2510NT", "EM2510NT-NEW",
            "EM2510NT-ASR", "EM2510NT-4", "EM23510NT",
            "FOMIIRI3015NT", "EML3510NT-TK", "ASR48M-TK",
            "ACIES2512-MTK", "ACIES2512-MTK2", "FLC2412AJ",
            "LC2012C1NT", "HDS8025NT-1", "HDS8025NT-2", "HDS8025NT-3",
            "HDS8025NT-4", "HDS8025NT-5", "HDS8025NT-6", "HDS8025NT-7",
            "HDS8025NT-8","HDS8025NT-9","HDS8025NT-10","HDS8025NT-11","HDS1303NT-1",
            "HDS1303NT-2","HD1703LNT","FBDITI1025NT-1","HG2204","HG-ATC-1","HG-ATC-2",
            "HG-ATC-3","HG-ATC-4","HG-Ars"};
        string machine_text_path = "";
        public Form1()
        {
            create_Textfile();
            InitializeComponent();
        }
        void create_Textfile()
        {

            foreach (var mname in machine_array)
            {
                try
                {
                    var values = new NameValueCollection
                          {
                              {"mname",mname }

                          };
                    WebClient client_machinetext = new WebClient();
                    string url = BuildUri("http://localhost:8080/misc/getMachineHistoryStatus.php?", values).ToString();
                    //client_machinetext.OpenReadCompleted += new OpenReadCompletedEventHandler(mText_client_DownloadFileCompleted);
                    client_machinetext.OpenReadAsync(new Uri(url));
                    machine_text_path = @"D:\xampp\htdocs\misc\" + mname + ".txt";
                }
                catch (Exception ex)
                {
                }

            }

        }
        void mText_client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                WebClient webclient = new WebClient();
                
                    if (!String.IsNullOrEmpty(machine_text_path))
                    {
                        byte[] responseArray = webclient.UploadFile(@"http://alfadock-pro.com/alfadockpro/services/save_vfactory.php", machine_text_path);
                        string response = System.Text.Encoding.ASCII.GetString(responseArray);
                    MessageBox.Show(response);
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem in saving session to alfaDOCK:" + ex.ToString());
            }

        }
        public Uri BuildUri(string root, NameValueCollection query)
        {

            // sneaky way to get a HttpValueCollection (which is internal)
            var collection = HttpUtility.ParseQueryString(string.Empty);

            foreach (var key in query.Cast<string>().Where(key => !string.IsNullOrEmpty(query[key])))
            {
                collection[key] = query[key];
            }

            var builder = new UriBuilder(root) { Query = collection.ToString() };
            return builder.Uri;
        }
    }
}
